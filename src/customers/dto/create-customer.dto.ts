import { IsNotEmpty, MinLength, IsInt, Min } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  @IsInt()
  @Min(1)
  age: number;

  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  gender: string;
}
